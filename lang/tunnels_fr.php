<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_aucun_tunnel' => 'Aucun tunnel détecté',
	'info_aucune_etape' => 'Aucune étape détectée',
	
	// N
	'noisette_options_afficher_titre_tunnel_label_case' => 'Inclure le titre général du tunnel (balise h1)',
	'noisette_options_tunnel_label' => 'Choix du tunnel',
	
	// T
	'titre_etapes' => 'Étapes',
	'titre_tunnel' => 'Tunnel',
	'titre_tunnels' => 'Tunnels',
	'titre_tunnels_detectes' => 'Liste des tunnels détectés :',

);
