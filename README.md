# Plugin Tunnels

![](images/plugin-tunnels.svg)

> Un plugin pour implémenter facilement des tunnels

## Principe

Un tunnel est une série d’étapes à faire dans un ordre précis. C’est en  quelque sorte l’équivalent d’un formulaire multi-étapes, mais découpé en plusieurs  formulaires indépendants sur plusieurs pages. Il peut s’agir de tunnels pour passer une commande, pour s’abonner, etc.

Ce plugin fournit un mécansime pour décrire les tunnels et leurs étapes, ainsi qu'un squelette pour l'affichage avec tous les automatismes nécessaires (menu de navigation, gestion des autorisations, redirections éventuelles, etc).

Les règles :

* Les étapes se font dans un ordre précis
* On peut revenir en arrière à tout moment
* Pour accéder à une étape, il faut avoir le droit d'accéder à toutes les étapes antérieures.
* Lorsqu'on essaie d'accéder à une étape interdite, on est redirigé vers la dernière étape autorisée


## Mise en oeuvre

### 1) Créer les squelettes de chaque étape

Chaque étape doit avoir son squelette.
Ils sont à placer dans le dossier `inclure/tunnel/`

On peut créer un sous-dossier pour chaque tunnel déclaré, les squelettes seront cherchés en priorité dans celui-ci.

Autrement dit, laissez à la racine les squelettes pouvant être mutualisés entre tous les tunnels, et mettez les autres dans leurs sous-dossiers respectifs.

Exemple : login.html est une étape commune à tous les tunnels, ensuite chacun a ses propres squelettes d'étapes.

```
inclure
  |- tunnel
     |- login.html
     |- tunnel_A
       |- bidule.html
       |- machin.html
     |- tunnel_B
       |- lorem.html
       |- ipsum.html
```

Dans ces squelettes, vous avez à disposition ces variables d'environnement :

* tunnel : identifiant du tunnel
* etape : identifiant de l'étape
* etapes : tableau de toutes les étapes avec leurs titres et leurs URLs
* etape_precedente : identifiant de l'étape précédente
* etape_suivante : identifiant de l'étape suivante
* url_etape_suivante : URL de l'étape suivante
* url_etape_precedente : URL de l'étape précédente

La plus importante étant `url_etape_suivante` qu’il faut passer en paramètre aux différents formulaire, pour la redirection.

### 2) Déclarer les tunnels et leurs étapes

Les tunnels se déclarent au moyen du pipeline `decrire_tunnels`.
Celui-ci doit renvoyer un tableau avec l'identifiant du tunnel, son titre et la liste de ses étapes, sous la forme `nom du squelette => titre`.

```php
array(
	'tunnelA' => array(
		'titre' => 'Titre du tunnel',
		'etapes' => array(
			'truc' => 'Titre de l’étape Truc',
			'muche' => 'Titre de l’étape Muche',
		),
	),
	'tunnelB' => array(...),
);
```

### 3) Définir les autorisations

Enfin, il reste à définir les autorisations pour chaque étape.

Il existe deux types d'autorisation, les deux autorisées par défaut :

* lister une étape, c'est-à-dire la voir dans le chemin, qu'elle soit visible
* voir le contenu d'une étape, c'est-à-dire y accéder, pouvoir s'y rendre réellement

#### Lister une étape
Par défaut, toutes les étapes déclarées d'un tunnel sont listées, visibles.

Mais on peut créer une fonction d'autorisation propre à un tunnel précis pour contrôler la visibilité de ses étapes :
`autoriser_{identifiant du tunnel}_lister_etape_tunnel`

À l'intérieur, l'argument `$id` contient l'identifiant de l'étape à tester.

#### Voir le contenu d'une étape
Celles-ci se cumulent : c’est à dire qu’à l'étape n°3 par exemple, on teste les autorisations des étapes n°1 et n°2. Il est donc inutile de refaire les tests des étapes précédentes dans les autorisations.

Les autorisations doivent être nommées de la sorte : `autoriser_tunnel{identifiant du tunnel}{identifiant de l’étape}_voir`.

Par exemple pour l'étape `panier` du tunnel `commande` : `autoriser_tunnelcommandepanier_voir`

Si le squelette d'une étape comporte les caractères `_` ou `-`, il faut les retirer. Par exemple l’autorisation pour un squelette `truc_muche` serait `autoriser_tunnelcommandetrucmuche_voir`.

_Dans une prochaine version V1 stable, ces autorisations suivront une nomenclature similaire à la précédente, avec une seule fonction par tunnel._

### 4) Intégrer le tunnel

Enfin, vous pouvez intégrer le tunnel sur n'importe quelle page.
Il suffit d'inclure un squelette en précisant l'identifiant du tunnel souhaité :

```html
<INCLURE{fond=inclure/tunnel, tunnel=commande, env}>
```

## Exemple

Un exemple pour un tunnel de commande se reposant sur les plugins [Commandes](https://contrib.spip.net/Commandes-4527), [Paniers](https://plugins.spip.net/paniers.html), [Bank](http://contrib.spip.net/4627), et [Profils](https://contrib.spip.net/5124) pour les informations utilisateur.

Il y aurait 3 étapes :

1. Validation du panier
2. Inscription ou mise à jour des informations utilisateur
3. Paiement

### 1) Squelettes

**inclure/tunnel/panier.html**

```html
#FORMULAIRE_PANIER{#ENV{url_etape_suivante}}
```

**inclure/tunnel/profil.html**

```html
[(#REM) Pour les connecté⋅e⋅s : mise à jour du profil ]
[(#SESSION{id_auteur}|oui)
	#FORMULAIRE_PROFIL{#SESSION{id_auteur,'',#ENV{url_etape_suivante}}
]

[(#REM) Pour les anonymes : connexion ou inscription ]
	#FORMULAIRE_INSCRIPTION{6forum, 0, #ENV{url_etape_suivante}}
	#FORMULAIRE_LOGIN{#SELF}
]
```

**inclure/tunnel/paiement.html**

```html
<BOUCLE_commande(COMMANDES)
	{id_auteur=#SESSION{id_auteur}}
	{statut=encours}
	{0,1}
	{!par date}
	{tout}
>
[(#FORMULAIRE_PAYER_ACTE{#PRIX*, #ARRAY{
	montant_ht,  #PRIX_HT*,
	id_auteur,   #ID_AUTEUR,
	id_commande, #ID_COMMANDE,
}})]
</BOUCLE_commande>
```

### 2) pipeline

```php
function monplugin_decrire_tunnels($tunnels) {

	$tunnels['commande'] = array(
		'titre' => _T('monplugin:titre_commander'),
		'etapes' => array(
			'panier'    => _T('monplugin:titre_etape_panier'),
			'profil'    => _T('monplugin:titre_etape_profil'),
			'livraison' => _T('monplugin:titre_etape_livraison'),
			'paiement'  => _T('monplugin:titre_etape_paiement'),
		),
	);

	return $tunnels;
}
```

### 3) Autorisations

```php
// Visibilité des étapes
function autoriser_commande_lister_etape_tunnel_dist($faire, $quoi, $id, $qui, $options) {
	// Toutes les étapes sont visibles par défaut
	$ok = true;
	
	// Pour la livraison, on teste plus en détail
	if ($id == 'livraison') {
		// S'il n'y a pas au moins un produit physique dans la commande
		if (…) {
			// L'étape n'est même pas listée du tout
			$ok = false;
		}
	}

	return $ok;
}

// Etape panier : ok tout le temps
function autoriser_tunnelcommandepanier_voir_dist($faire, $quoi, $id, $qui, $options) {
	$autoriser = true;
	return $autoriser;
}

// Profil : avoir une commande en cours
function autoriser_tunnelabonnementabonnementprofil_voir($faire, $quoi, $id, $qui, $options) {
	include_spip('inc/session');
    $has_commande = sql_countsel(
    	'spip_commandes',
    	array(
    		'statut=' . sql_quote('encours'),
    		'id_auteur=' . intval($qui['id_auteur']),
    	)
    );
	$autoriser = $has_commande;
	return $autoriser;
}

// Paiement : avoir mis à jour son profil
// TODO
```