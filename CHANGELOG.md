# Changelog

## 0.5.0 - 2023-10-04

### Added

* Possibilité de ranger les étapes spécifiques à un tunnel dans un sous-dossier `inclure/tunnel/<tunnel>/mon_etape.html`