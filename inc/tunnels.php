<?php
/**
 * Fonctions utiles aux tunnels
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Donne toutes les infos utiles sur un tunnel et l'étape en cours
 *
 * @uses tunnels_lister_tunnel
 * @uses tunnels_determiner_tunnel
 * @uses tunnels_determiner_etape
 * @uses tunnels_etape_suivante
 * @uses tunnels_etape_precedente
 *
 * @param String $tunnel
 *     Identifiant du tunnel demandé
 *     Défaut : 1er tunnel déclaré
 * @param String $etape
 *     Identifiant de l'étape demandée
 *     Défaut = 1ère étape
 * @param Array $options
 *     Options diverses
 *     - parametre_etape : nom de la query string pour l'étape, défaut = `e`
 * @return Array
 *     En cas d'erreur, un tableau vide (si aucun tunnel ou étape)
 *     Si ok, un tableau associatif :
 *     - tunnel : tunnel déterminé, le 1er trouvé s'il n'est pas précisé
 *     - etape : étape déterminée, qui peut être différente de celle demandée
 *     - etape_suivante
 *     - etape_precedente
 *     - url_etape_suivante
 *     - url_etape_precedente
 *     - titre_tunnel
 *     - titre_etape
 */
function tunnels_infos($tunnel = '', $etape = '', $options = array()) {
	$infos = array();
	if (
		$tunnel_determine = tunnels_determiner_tunnel($tunnel)
		and $etape_determinee = tunnels_determiner_etape($tunnel_determine, $etape, $options)
	) {
		include_spip('inc/utils');
		include_spip('inc/autoriser');
		$tunnels = tunnels_decrire_tunnels();
		$etapes  = tunnels_lister_etapes($tunnel_determine);

		// La base
		$infos['tunnel'] = $tunnel_determine;
		$infos['etape']  = $etape_determinee;

		// Étapes adjacentes et URLS
		$urls_etapes = array();
		$parametre_etape = !empty($options['parametre_etape']) ? $options['parametre_etape'] : 'e';
		$infos['etape_suivante']       = tunnels_etape_suivante($tunnel_determine, $etape_determinee, $options);
		$infos['etape_precedente']     = tunnels_etape_precedente($tunnel_determine, $etape_determinee, $options);
		$infos['url_etape_suivante']   = parametre_url(self(), $parametre_etape, $infos['etape_suivante']);
		$infos['url_etape_precedente'] = parametre_url(self(), $parametre_etape, $infos['etape_precedente']);

		// Étapes avec titre + URL + les autorisations déjà calculées
		$etapes_plus = array();
		$autoriser = true;
		foreach ($etapes as $etape => $titre) {
			// On ne liste cette étape que si elle est bien listable
			if (autoriser('lister_etape_tunnel', "_$tunnel_determine", $etape, null, $options)) {
				$autoriser = ($autoriser and autoriser('voir', "_tunnel$tunnel_determine$etape", 0, null, $options));
				$etape_suivante = tunnels_etape_suivante($tunnel_determine, $etape, $options);
				$etapes_plus[$etape] = array(
					'titre' => $titre,
					'url'   => parametre_url(self(), $parametre_etape, $etape),
					'est_autorise' => $autoriser,
					'est_complete' => ($autoriser and $etape_suivante and autoriser('voir', "_tunnel$tunnel_determine$etape_suivante", 0, null, $options)),
					'est_active' => $est_active = ($etape == $etape_determinee),
					'est_cliquable' => !$est_active and $autoriser,
				);
			}
		}
		$infos['etapes'] = $etapes_plus;

		// Titres
		$infos['titre_tunnel'] = !empty($tunnels[$tunnel_determine]['titre']) ? $tunnels[$tunnel_determine]['titre'] : '';
		$infos['titre_etape'] = !empty($tunnels[$tunnel_determine]['etapes'][$etape_determinee]) ? $tunnels[$tunnel_determine]['etapes'][$etape_determinee] : '';

	} else {
		spip_log("Pas de tunnel et d’étape valables, demandés : tunnel `$tunnel` / étape `$etape`, déterminés : tunnel `$tunnel_determine` / étape `$etape_determinee`", 'tunnels.' . _LOG_ERREUR);
	}

	return $infos;
}


/**
 * Lister les tunnels
 *
 * Ils sont déclarés via le pipeline `decrire_tunnels`.
 *
 * @static
 * @uses pipeline()
 *
 * @return Array
 *      Tableau associatif
 *      Les clés sont les identifiants des tunnels.
 *      Les sous-tableau sont les étapes du tunnel, avec des paires
 *      squelette / nom humain de l'étape.
 */
function tunnels_decrire_tunnels() {

	static $tunnels;
	if (is_array($tunnels)) {
		return $tunnels;
	}

	// Les tunnels sont déclarés via un pipeline
	if (
		!$tunnels = pipeline('decrire_tunnels', array())
		or !is_array($tunnels)
	) {
		$tunnels = array(); // au cas où des plugins renverraient du caca
		spip_log('Aucun tunnel n’est déclaré via le pipeline decrire_tunnels', 'tunnels.' . _LOG_ERREUR);
	}

	return $tunnels;
}


/**
 * Lister les étapes d'un tunnel
 *
 * @static
 * @uses tunnels_decrire_tunnels()
 *
 * @param String $tunnel
 *      Identifiant du tunnel
 * @return Array
 *      Tableau associatif
 *      Paires squelette / nom humain de l'étape.
 */
function tunnels_lister_etapes($tunnel) {

	static $etapes_tunnels;
	if (isset($etapes_tunnels[$tunnel])) {
		return $etapes_tunnels[$tunnel];
	}

	$etapes = array();
	if (
		$tunnels = tunnels_decrire_tunnels($tunnel)
		and isset($tunnels[$tunnel]['etapes'])
	) {
		$etapes = $tunnels[$tunnel]['etapes'];
		$etapes_tunnels[$tunnel] = $etapes;
	}

	return $etapes;
}


/**
 * Détermine le type de tunnel utilisé
 *
 * Par défaut on prend le 1er tunnel déclaré
 *
 * @uses tunnels_decrire_tunnels()
 *
 * @param String $tunnel
 *     Identifiant du tunnel demandé
 * @return String
 */
function tunnels_determiner_tunnel($tunnel = '') {

	$tunnels = array_keys(tunnels_decrire_tunnels());

	// Par défaut on prend le 1er
	if (!$tunnel) {
		$tunnel = $tunnels[0];
	}

	// Vérifier que le tunnel demandé existe
	if (
		!$tunnels
		or !in_array($tunnel, $tunnels)
	) {
		$tunnel = '';
	}

	return $tunnel;
}


/**
 * Détermine l'étape courante d'un tunnel selon les autorisations
 *
 * L'étape retournée n'est pas nécessairement celle demandée.
 *
 * Principe :
 * On demande une étape, et on vérifie si on a le droit d'y accéder.
 * Les étapes doivent toutes être autorisées jusqu'à celle demandée.
 * Dès qu'il y a une interdiction, on ramène à l'étape précédent celle interdite.
 * Autrement dit, les autorisations s'additionnent : les tests faits
 * pour une étape sont valables pour les suivantes, pas besoin de les répéter
 * dans les fonctions d'autorisations.
 *
 * Si l'étape demandée n'existe pas, on prend la 1ère.
 *
 * @uses tunnels_lister_etapes()
 * @uses autoriser()
 *
 * @param String $tunnel
 *     Identifiant du tunnel
 * @param string $etape
 *     Étape demandée
 * @return string
 *     Étape déterminée par nous selon les autorisations
 *     Chaîne vide si aucune étape (pas d'autorisation par ex.)
 */
function tunnels_determiner_etape($tunnel, $etape = '', $options = array()) {
	static $etapes_determinees;
	$hash = "$tunnel$etape";
	if (isset($etapes_determinees[$hash])) {
		return $etapes_determinees[$hash];
	}

	include_spip('inc/autoriser');
	$etapes = tunnels_lister_etapes($tunnel);

	// S'il n'y a pas d'étape valide demandée, on prend la 1ère
	if (
		!$etape
		or !in_array($etape, array_keys($etapes))
	) {
		$etape = array_shift(array_keys($etapes));
	}

	// Les étapes doivent toutes être autorisées jusqu'à celle demandée.
	// On teste les autorisations dans l'ordre,
	// dès qu'il y a une interdiction, on s'arrête et on revient à la précédente.
	$etape_determinee    = $etape; // on commence en étant optimiste
	$cle_etape           = array_search($etape, array_keys($etapes));
	$etapes_a_tester     = array_keys(array_slice($etapes, 0, $cle_etape+1, true)); // couper après l'étape demandée
	foreach ($etapes_a_tester as $k => $etape_test) {
		if (
			// Si l'étape à tester n'est PAS masquée, on doit tester si on a le droit d'y accéder
			(
				$lister = autoriser('lister_etape_tunnel', "_$tunnel", $etape_test, null, $options)
				and !$autoriser = autoriser('voir', "_tunnel$tunnel$etape_test", 0, null, $options)
			)
			// Si l'étape est une masquée, on bloque seulement si c'était celle demandée au départ
			or (
				!$lister and $etape_test == $etape
			)
		) {
			$num_etape = $cle_etape + 1;
			// si c'est la 1ère étape, pas de bol
			if ($k === 0) {
				$etape_determinee = '';
				spip_log("Tunnel `$tunnel` : étape n°$num_etape `$etape` non autorisée, pas de fallback", 'tunnels');
			// sinon on prend l'étape précédente
			} else {
				$num_etape_determinee = $k;
				$etape_determinee = $etapes_a_tester[$k - 1];
				spip_log("Tunnel `$tunnel` : étape n°$num_etape `$etape` non autorisée, fallback sur étape n°$num_etape_determinee `$etape_determinee`", 'tunnels');
			}
			break;
		}
	}

	$etapes_determinees[$hash] = $etape_determinee;

	return $etape_determinee;
}

/**
 * Renvoie l'étape suivante relativement à une étape donnée, si elle existe
 *
 * @uses tunnels_lister_etapes()
 *
 * @param String $tunnel
 * @param String $etape
 * @return String
 */
function tunnels_etape_suivante($tunnel, $etape, $options=array()) {
	$etape_suivante = '';
	
	if ($etapes = array_keys(tunnels_lister_etapes($tunnel))) {
		$cle_etape = array_search($etape, $etapes);
		$cle_etape_suivante = min(count($etapes) - 1, $cle_etape + 1); // On avance, mais sans dépasser la dernière étape
		// Tant que l'étape suivant trouvée n'est PAS listable, on cherche de nouveau la suivante plus loin
		while (!autoriser('lister_etape_tunnel', "_$tunnel", $etapes[$cle_etape_suivante], null, $options)) {
			$cle_etape_suivante = min(count($etapes) - 1, $cle_etape_suivante + 1); // On avance, mais sans dépasser la dernière étape
		}
		if ($cle_etape != $cle_etape_suivante) {
			$etape_suivante = $etapes[$cle_etape_suivante];
		}
	}

	return $etape_suivante;
}

/**
 * Renvoie l'étape précédente relativement à une étape donnée, si elle existe
 *
 * @uses tunnels_lister_etapes()
 *
 * @param String $tunnel
 * @param String $etape
 * @return String
 */
function tunnels_etape_precedente($tunnel, $etape, $options=array()) {
	$etape_precedente = '';
	
	if ($etapes = array_keys(tunnels_lister_etapes($tunnel))) {
		$cle_etape = array_search($etape, $etapes);
		$cle_etape_precedente = max(0, $cle_etape - 1); // On rembobine, mais pas en dessous de 0
		// Tant que l'étape précédente trouvée n'est PAS listable, on cherche de nouveau la précédente plus loin
		while (!autoriser('lister_etape_tunnel', "_$tunnel", $etapes[$cle_etape_precedente], null, $options)) {
			$cle_etape_precedente = max(0, $cle_etape_precedente - 1); // On rembobine, mais pas en dessous de 0
		}
		if ($cle_etape != $cle_etape_precedente) {
			$etape_precedente = $etapes[$cle_etape_precedente];
		}
	}

	return $etape_precedente;
}
